NAME
    POE::Component::PubSub - A generic publish/subscribe POE::Component that
    enables POE::Sessions to publish events to which other POE::Sessions may
    subscribe.

VERSION
    Version 0.04

SYNOPSIS
    # Instantiate the publish/subscriber with the alias "pub"
    POE::Component::PubSub->new('pub');

    # Publish an event called "FOO". +PUBLISH_OUTPUT is actually optional.
    $_[KERNEL]->post('pub', 'publish', 'FOO', +PUBLISH_OUTPUT);

    # Elsewhere, subscribe to that event, giving it an event to call # when
    the published event is fired. $_[KERNEL]->post('pub', 'subscribe',
    'FOO', 'FireThisEvent');

    # Fire off the published event $_[KERNEL]->post('pub', 'FOO');

    # Publish an 'input' event $_[KERNEL]->post('pub', 'publish', 'BAR',
    +PUBLISH_INPUT, 'MyInputEvent');

    # Tear down the whole thing $_[KERNEL]->post('pub', 'destroy');

EVENTS
    All public events do some sanity checking to make sure of a couple of
    things before allowing the events such as checking to make sure the
    posting session actually owns the event it is publishing, or that the
    event passed as the return event during subscription is owned by the
    sender. When one of those cases comes up, an error is carp'd, and the
    event returns without stopping execution.

    'publish'
        This is the event to use to publish events. It accepts one argument,
        the event to publish. The published event may not already be
        previously published. The event may be completely arbitrary and does
        not require the publisher to implement that event. Think of it as a
        name for a mailing list.

        You can also publish an 'input' or inverse event. This allows for
        arbitrary sessions to post to your event. In this case, you must
        supply the optional published event type and the event to be called
        when the published event fires.

        There are two types: PUBLISH_INPUT and PUBLISH_OUTPUT. PUBLISH_OUPUT
        is implied when no argument is supplied.

    'subscribe'
        This is the event to use when subscribing to published events. It
        accepts two arguments: 1) the published event, and 2) the event name
        of the subscriber to be called when the published event is fired.
        The event doesn't need to be published prior to subscription to
        resolve chicken and egg problems in an async environment. But, the
        sender must own and implement the return event.

    'rescind'
        Use this event to stop publication of an event. It accepts one
        argument, the published event. The event must be published, and
        published by the sender of the rescind event. If the published event
        has any subscribers, a warning will be carp'd but execution will
        continue.

    'cancel'
        Cancel subscriptions to events with this event. It accepts one
        argment, the published event. The event must be published and the
        sender must be subscribed to the event.

    '_default'
        After an event is published, the publisher may arbitrarily fire that
        event to this component and the subscribers will be notified by
        calling their respective return events with whatever arguments are
        passed by the publisher. The event must be published, owned by the
        publisher, and have subscribers for the event to be propagated. If
        any of the subscribers no longer has a valid return event their
        subscriptions will be cancelled and a warning will be carp'd.

    'listing'
        To receive an array reference containing tuples of the event name,
        and the type of the events that are currently published within the
        component, call this event. It accepts one argument, the return
        event to fire with the listing. The sender must own the return
        event.

    'destroy'
        This event will simply destroy any of its current state and remove
        any and all aliases this session may have picked up. This should
        free up the session for garbage collection.

CLASS METHODS
    POE::Component::PubSub->new($alias)
        This is the constructor for the publish subscribe component. It
        instantiates it's own session using the provided $alias argument to
        set its kernel alias. If no alias is provided, the default alias is
        'PUBLISH_SUBSCRIBE'.

DEBUGGING
    $POE::Component::PubSub::TRACE_AND_DEBUG
        To enable debugging within the component at the POE::Session level
        and also with various warnings, set this variable to logical true
        BEFORE calling new().

NOTES
    Right now this component is extremely simple, but thorough when it comes
    to checking the various requirements for publishing and subscribing.
    Currently, there is no mechanism to place meta-subscriptions to the
    events of the component itself. This feature is planned for the next
    release.

    Also, to do some of the checking on whether subscribers own the return
    events, some ideas were lifted from POE::API::Peek, and like that
    module, if there are changes to the POE core, they may break this
    module.

AUTHOR
    Nicholas R. Perez, "<nperez at cpan.org>"

BUGS
    Please report any bugs or feature requests to "bug-poe-component-pubsub
    at rt.cpan.org", or through the web interface at
    <http://rt.cpan.org/NoAuth/ReportBug.html?Queue=POE-Component-PubSub>. I
    will be notified, and then you'll automatically be notified of progress
    on your bug as I make changes.

SUPPORT
    You can find documentation for this module with the perldoc command.

        perldoc POE::Component::PubSub

    You can also look for information at:

    *   AnnoCPAN: Annotated CPAN documentation

        <http://annocpan.org/dist/POE-Component-PubSub>

    *   CPAN Ratings

        <http://cpanratings.perl.org/d/POE-Component-PubSub>

    *   RT: CPAN's request tracker

        <http://rt.cpan.org/NoAuth/Bugs.html?Dist=POE-Component-PubSub>

    *   Search CPAN

        <http://search.cpan.org/dist/POE-Component-PubSub>

COPYRIGHT & LICENSE
    Copyright 2008-2009 Nicholas R. Perez, all rights reserved.

    This program is released under the following license: gpl

